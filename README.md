This is a TensorFlow example implementation for image classification with pretrained imagenet.


## Predicting an image

In order to perform example image classifcation the network, execute
```bash
python3 classify_image_net.py --image=cropped_panda.jpeg

output:
giant panda, panda, panda bear, coon bear, Ailuropoda melanoleuca (score = 0.89107)
indri, indris, Indri indri, Indri brevicaudatus (score = 0.00779)
lesser panda, red panda, panda, bear cat, cat bear, Ailurus fulgens (score = 0.00296)
custard apple (score = 0.00147)
earthstar (score = 0.00117)
```
or

```bash
python3 classify_image_net.py --image=dog.jpeg

output:
collie (score = 0.41246)
Border collie (score = 0.33497)
Saint Bernard, St Bernard (score = 0.07620)
Cardigan, Cardigan Welsh corgi (score = 0.01751)
Great Pyrenees (score = 0.01188)
```






## Model files and loading routine

The model is automatically downloaded to ./imagenet and consists of an single protobuf file classify_image_graph_def.pb. This file contains the model graph definition as well as the trained model parameters. We load this file the create_graph() function.

## Image inference code
The entire prediction process takes place in run_inference_on_image(image). We load the image to classify with
tensorflow and feed it to the model to obtain a prediction. This prediction will then be converted by the NodeLookup class
to a human interpretable string.
